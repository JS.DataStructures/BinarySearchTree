import chai from 'chai';
import BinarySearchTree from '../src/BinarySearchTree';
import traverse from '../src/postordertraversal';

const expect = chai.expect;

describe('Post-Order Depth Traversal', () => {
    let binaryTree;
    beforeEach(() => {
        binaryTree = new BinarySearchTree(100);
        binaryTree.insert(50);
        binaryTree.insert(150);
        binaryTree.insert(25);
        binaryTree.insert(75);
        binaryTree.insert(175);
        binaryTree.insert(125);
        binaryTree.insert(20);
        binaryTree.insert(30);
        binaryTree.insert(70);
        binaryTree.insert(80);
        binaryTree.insert(120);
        binaryTree.insert(130);
        binaryTree.insert(170);
        binaryTree.insert(180);
    });
    describe('Post-Order Tree traversal', () => {
        it('should traverse the tree', () => {
            const traversedOutput = traverse(binaryTree.rootNode);
            expect(traversedOutput.toString()).to.equal('20,30,25,70,80,75,50,120,130,125,170,180,175,150,100');
        });
    });
});
