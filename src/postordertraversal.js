export default function traverse(rootNode) {
    let traversalOutput = [];
    if (rootNode === null) {
        return traversalOutput;
    }
    traversalOutput = traversalOutput.concat(traverse(rootNode.leftChild));
    traversalOutput = traversalOutput.concat(traverse(rootNode.rightChild));
    traversalOutput.push(rootNode.value);
    return traversalOutput;
}
