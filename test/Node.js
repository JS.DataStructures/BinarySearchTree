import chai from 'chai';
import Node from '../src/Node';

const expect = chai.expect;

describe('Node Tests', () => {
    let node;
    beforeEach(() => {
        node = new Node(100);
    });
    describe('Height/Depth Test', () => {
        it('should calculate the height', () => {
            node.leftChild = new Node(50);
            node.rightChild = new Node(150);
            node.leftChild.leftChild = new Node(25);

            expect(node.height).to.equal(2);
        });
        it('should calculate the depth', () => {
            node.leftChild = new Node(50);
            node.rightChild = new Node(150);
            node.leftChild.leftChild = new Node(25);
            node.leftChild.leftChild.leftChild = new Node(20);

            expect(node.leftChild.leftChild.leftChild.getDepthFromNode(node)).to.equal(3);
        });
    });
})