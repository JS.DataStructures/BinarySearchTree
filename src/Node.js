export default class Node {
    constructor(value) {
        this._data = {
            _leftChild: null,
            _rightChild: null,
            _value: null
        };
        this.leftChild = null;
        this.rightChild = null;
        this.value = value;
    }
    getDepthFromNode(node) {
        if (node === null) {
            throw new Error('The tree doesn\'t contain the current node');
        }
        if (!(node instanceof Node)) {
            throw new Error('Invalid root node data type');
        }
        if (node.value === this.value) {
            return 0;
        }
        let depth = 1;
        if (node.value > this.value) {
            depth += this.getDepthFromNode(node.leftChild);
        } else {
            depth += this.getDepthFromNode(node.rightChild);
        }
        return depth;
    }
    get height() {
        // If this is a leaf node, return -1
        if (this.leftChild === null && this.rightChild === null) {
            return -1;
        }
        const leftChildHeight = this.leftChild ? this.leftChild.height : 0;
        const rightChildHeight = this.rightChild ? this.rightChild.height : 0;
        return Math.max(leftChildHeight, rightChildHeight) + 1;
    }
    get maximum() {
        if (this.rightChild === null) {
            return this.value;
        }
        return this.rightChild.maximum;
    }
    get minimum() {
        if (this.leftChild === null) {
            return this.value;
        }
        return this.leftChild.minimum;
    }
    get leftChild() {
        return this._data._leftChild;
    }
    get rightChild() {
        return this._data._rightChild;
    }
    get value() {
        return this._data._value;
    }
    set leftChild(node) {
        if (!(node instanceof Node) && node !== null) {
            throw new Error('Invalid child data type');
        }
        this._data._leftChild = node;
    }
    set rightChild(node) {
        if (!(node instanceof Node) && node !== null) {
            throw new Error('Invalid child data type');
        }
        this._data._rightChild = node;
    }
    set value(value) {
        if (typeof value !== 'number') {
            throw new Error('Invalid value data type');
        }
        this._data._value = value;
    }
}
