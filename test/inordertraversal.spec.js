import chai from 'chai';
import BinarySearchTree from '../src/BinarySearchTree';
import traverse from '../src/inordertraversal';

const expect = chai.expect;

describe('In-Order Depth Traversal', () => {
    let binaryTree;
    beforeEach(() => {
        binaryTree = new BinarySearchTree(100);
        binaryTree.insert(50);
        binaryTree.insert(150);
        binaryTree.insert(25);
        binaryTree.insert(75);
        binaryTree.insert(175);
        binaryTree.insert(125);
        binaryTree.insert(20);
        binaryTree.insert(30);
        binaryTree.insert(70);
        binaryTree.insert(80);
        binaryTree.insert(120);
        binaryTree.insert(130);
        binaryTree.insert(170);
        binaryTree.insert(180);
    });
    describe('In-Order Tree traversal', () => {
        it('should traverse the tree', () => {
            const traversedOutput = traverse(binaryTree.rootNode);
            expect(traversedOutput.toString()).to.equal('20,25,30,50,70,75,80,100,120,125,130,150,170,175,180');
        });
    });
});
