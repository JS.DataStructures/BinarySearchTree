import Node from './Node';

export default class BinarySearchTree {
    constructor(value) {
        this._data = {
            _rootNode: null
        };
        this.rootNode = this.getNewNode(value);
    }
    getNewNode(value) {
        const node = new Node(value);
        node.depth = () => {
            const depth = node.getDepthFromNode(this.rootNode);
            return depth;
        };
        node.getParent = () => {
            const parent = this.getParentNode(node.value);
            return parent;
        };
        node.delete = () => {
            const deleteReturn = this.delete(node.value);
            return deleteReturn;
        };
        node.rotateLeft = () => {
            this.rotateNodeLeft(node);
        };
        node.rotateRight = () => {
            this.rotateNodeRight(node);
        };
        return node;
    }
    insert(value) {
        let parentNode = this.rootNode;
        let valueInsertedIntoTree = false;
        // We can do this recursively; however, I don't want insert to take in a tree/subtree parameter.
        do {
            if (parentNode.value > value) {
                if (parentNode.leftChild === null) {
                    parentNode.leftChild = this.getNewNode(value);
                    valueInsertedIntoTree = true;
                } else {
                    parentNode = parentNode.leftChild;
                }
            } else if (parentNode.rightChild === null) {
                parentNode.rightChild = this.getNewNode(value);
                valueInsertedIntoTree = true;
            } else {
                parentNode = parentNode.rightChild;
            }
        } while (valueInsertedIntoTree === false);
    }
    delete(value) {
        const node = this.find(value);
        if (node === null) {
            return node;
        }
        const parentNode = node.getParent();
        const isRootNode = (parentNode === null);
        const nodeLeftOfParent = (!isRootNode && parentNode.value > value);
        if (node.leftChild === null && node.rightChild === null) {
            // node is a leaf node
            if (isRootNode) {
                throw new Error('Cannot delete the root node without any children');
            }
            if (nodeLeftOfParent) {
                parentNode.leftChild = null;
            } else {
                parentNode.rightChild = null;
            }
        } else if (node.leftChild === null || node.rightChild === null) {
            // node has one child
            const newNode = node.leftChild === null ? node.rightChild : node.leftChild;
            if (nodeLeftOfParent) {
                parentNode.leftChild = newNode;
            } else if (isRootNode) {
                this.rootNode = newNode;
            } else {
                parentNode.rightChild = newNode;
            }
        } else {
            // node has two children
            const maxValue = node.leftChild.maximum;
            this.delete(maxValue);
            node.value = maxValue;
        }
        return value;
    }
    isBinarySearchTree = (startingNode = this.rootNode, minimum = null, maximum = null) => {
        if (startingNode === null) {
            // We have traversed to the leaf node; hence, it's bound to be safe - return true
            return true;
        }
        const nodeData = startingNode.data;
        const minimumPossible = minimum === null ? nodeData - 1 : minimum;
        const maximumPossible = maximum === null ? nodeData + 1 : maximum;

        if (nodeData < minimumPossible || nodeData > maximumPossible) {
            return false;
        }

        return this.isBinarySearchTree(startingNode.leftChild, minimum, nodeData)
            && this.isBinarySearchTree(startingNode.rightChild, nodeData, maximum);
    }
    getParentNode(value) {
        if (!this.contains(value)) {
            // The value is not present in the tree
            return null;
        }
        let parentNode = this.rootNode;
        if (parentNode.value === value) {
            return null;
        }
        while (parentNode !== null) {
            if (parentNode.value > value) {
                if (parentNode.leftChild.value === value) {
                    return parentNode;
                }
                parentNode = parentNode.leftChild;
            } else if (parentNode.rightChild.value === value) {
                return parentNode;
            } else {
                parentNode = parentNode.rightChild;
            }
        }
        return null;
    }
    rotateNodeRight(node) {
        const isRootNode = (this.rootNode === node);

        const newCenter = node.leftChild;
        node.leftChild = newCenter.rightChild;
        newCenter.rightChild = node;

        if (isRootNode) {
            this.rootNode = newCenter;
        } else {
            const parentNode = this.getParentNode(node.value);
            if (parentNode.value > node.value) {
                parentNode.leftChild = newCenter;
            } else {
                parentNode.rightChild = newCenter;
            }
        }
    }
    rotateNodeLeft(node) {
        const isRootNode = (this.rootNode === node);

        const newCenter = node.rightChild;
        node.rightChild = newCenter.leftChild;
        newCenter.leftChild = node;

        if (isRootNode) {
            this.rootNode = newCenter;
        } else {
            const parentNode = this.getParentNode(node.value);
            if (parentNode.value > node.value) {
                parentNode.leftChild = newCenter;
            } else {
                parentNode.rightChild = newCenter;
            }
        }
    }
    find(value) {
        let parentNode = this.rootNode;
        // We can do this recursively, however, I don't want find to take in a tree/subtree parameter
        while (parentNode !== null) {
            if (parentNode.value === value) {
                return parentNode;
            }
            if (parentNode.value > value) {
                parentNode = parentNode.leftChild;
            } else {
                parentNode = parentNode.rightChild;
            }
        }
        return null;
    }
    contains(value) {
        return this.find(value) instanceof Node;
    }
    get height() {
        return this.rootNode.height;
    }
    get depth() {
        return this.height;
    }
    get maximum() {
        return this.rootNode.maximum;
    }
    get minimum() {
        return this.rootNode.minimum;
    }
    get rootNode() {
        return this._data._rootNode;
    }
    set rootNode(node) {
        if (!(node instanceof Node)) {
            throw new Error('Invalid root data type');
        }
        this._data._rootNode = node;
    }
}
