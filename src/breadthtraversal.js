import Queue from 'queue';

export default function traverse(binaryTree) {
    const queue = new Queue();
    const traversedOutput = [];
    queue.push(binaryTree.rootNode);
    while (!queue.isEmpty()) {
        const node = queue.pop();
        traversedOutput.push(node.value);
        if (node.leftChild !== null) {
            queue.push(node.leftChild);
        }
        if (node.rightChild !== null) {
            queue.push(node.rightChild);
        }
    }
    return traversedOutput;
}
