import chai from 'chai';
import BinarySearchTree from '../src/BinarySearchTree';

const expect = chai.expect;

describe('Binary Tree Tests', () => {
    let binarySearchTree;
    beforeEach(() => {
        binarySearchTree = new BinarySearchTree(100);
    });
    describe('Insertion Tests', () => {
        it('should insert to the left', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(25);
            expect(binarySearchTree.rootNode.leftChild.leftChild.value).to.equal(25);
        });
        it('should insert to the right', () => {
            binarySearchTree.insert(150);
            binarySearchTree.insert(175);
            expect(binarySearchTree.rootNode.rightChild.rightChild.value).to.equal(175);
        });
        it('should insert left and right', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(75);
            expect(binarySearchTree.rootNode.leftChild.rightChild.value).to.equal(75);
        });
        it('should insert right and left', () => {
            binarySearchTree.insert(150);
            binarySearchTree.insert(125);
            expect(binarySearchTree.rootNode.rightChild.leftChild.value).to.equal(125);
        });
        it('should be able to construct', () => {
            expect(binarySearchTree).be.not.null && expect(binarySearchTree).be.not.undefined;
        });
        it('new binarySearchTree should not be empty', () => {
            expect(binarySearchTree.rootNode.value).to.equal(100);
        });
        it('the tree should contain the lower value insert', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            expect(binarySearchTree.contains(25)).to.equal(true);
        });
        it('the tree should contain the higher value insert', () => {
            binarySearchTree.insert(150);
            binarySearchTree.insert(125);
            binarySearchTree.insert(175);
            expect(binarySearchTree.contains(175)).to.equal(true);
        });
    });
    describe('Minimum Tests', () => {
        it('should be able to find the minimum value', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(150);
            binarySearchTree.insert(125);
            binarySearchTree.insert(175);
            expect(binarySearchTree.minimum).to.equal(25);
        });
    });
    describe('Maximum Tests', () => {
        it('should be able to find the maximum value', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(150);
            binarySearchTree.insert(125);
            binarySearchTree.insert(175);
            expect(binarySearchTree.maximum).to.equal(175);
        });
    });
    describe('Insertion/Contain/Find Tests', () => {
        it('should contain inserted value', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            expect(binarySearchTree.contains(75)).to.equal(true);
        });
        it('should not contain not inserted value', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            expect(binarySearchTree.contains(175)).to.equal(false);
        });
        it('should be able to find an inserted value', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            expect(binarySearchTree.find(75).value).to.equal(75);
        });
    });
    describe('Depth Tests', () => {
        it('should calculate the depth', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(20);
            expect(binarySearchTree.rootNode.leftChild.leftChild.leftChild.depth()).to.equal(3);
        });
    });
    describe('Parent Tests', () => {
        it('should be able to fetch the parent', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(20);
            expect(binarySearchTree.getParentNode(20).value).to.equal(25);
        });
        it('should return null if no parent', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(20);
            expect(binarySearchTree.getParentNode(120)).to.equal(null);
        });
        it('should be able to fetch the parent from the node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(20);
            expect(binarySearchTree.find(20).getParent().value).to.equal(25);
        });
        it('should find the parent of a left subtree leaf', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            expect(binarySearchTree.getParentNode(20).value).to.equal(25);
        });
        it('should find the parent of a left subtree internal node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            expect(binarySearchTree.getParentNode(75).value).to.equal(50);
        });
        it('should find the parent of a right subtree leaf', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            expect(binarySearchTree.getParentNode(180).value).to.equal(175);
        });
        it('should find the parent of a right subtree internal node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            expect(binarySearchTree.getParentNode(175).value).to.equal(150);
        });
    });
    describe('Delete Tests', () => {
        it('should delete a node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(20);
            expect(binarySearchTree.delete(20)).to.equal(20);
        });
        it('should delete a node from the node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(20);
            expect(binarySearchTree.find(20).delete()).to.equal(20);
        });
        it('should delete the root node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(20);
            binarySearchTree.delete(100);
            expect(binarySearchTree.rootNode.value).to.equal(50);
        });
        it('should be able to delete the left most leaf', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);

            binarySearchTree.delete(20);
            expect(binarySearchTree.contains(20)).to.equal(false);
        });
        it('should be able to delete the right most leaf', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);

            binarySearchTree.delete(180);
            expect(binarySearchTree.contains(180)).to.equal(false);
        });
        it('should be able to delete the right leaf of the left most internal node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);

            binarySearchTree.delete(30);
            expect(binarySearchTree.contains(30)).to.equal(false);
        });
        it('should be able to delete the left leaf of the right most internal node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);

            binarySearchTree.delete(170);
            expect(binarySearchTree.contains(170)).to.equal(false);
        });
        it('should be able to delete an internal node in the left subtree with only one child', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);

            binarySearchTree.delete(175);
            expect(binarySearchTree.getParentNode(170).value).to.equal(150);
        });
        it('should be able to delete an internal node in the right subtree with only one child', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);

            binarySearchTree.delete(25);
            expect(binarySearchTree.getParentNode(20).value).to.equal(50);
        });
        it('should be able to delete a node in the left subtree', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);

            binarySearchTree.delete(50);
            expect(binarySearchTree.rootNode.leftChild.value).to.equal(30);
        });
        it('should be able to delete a node in the right subtree', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);

            binarySearchTree.delete(150);
            expect(binarySearchTree.rootNode.rightChild.value).to.equal(130);
        });
        it('should be able to delete the root node with only one subtree', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);

            binarySearchTree.delete(100);
            expect(binarySearchTree.rootNode.value).to.equal(50);
        });
        it('should be able to delete the root node', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);

            binarySearchTree.delete(100);
            expect(binarySearchTree.rootNode.value).to.equal(80);
        });
    });
    describe('Is Binary Search Tree', () => {
        it('should be a binary search tree', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.insert(190);
            expect(binarySearchTree.isBinarySearchTree()).to.equal(true);
        });
    });
    describe('Rotate Left', () => {
        it('should rotate the root with only leaf children', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.rotateNodeLeft(binarySearchTree.rootNode);
            expect(binarySearchTree.rootNode.value).to.equal(150);
        });
        it('should rotate the root with nested children', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.insert(190);
            binarySearchTree.rotateNodeLeft(binarySearchTree.rootNode);
            expect(binarySearchTree.rootNode.value).to.equal(150);
        });
        it('should rotate the root with nested children and left right should be new', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.insert(190);
            binarySearchTree.rotateNodeLeft(binarySearchTree.rootNode);
            expect(binarySearchTree.rootNode.leftChild.rightChild.value).to.equal(125);
        });
        it('should rotate the node with only leaf children', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.rotateNodeLeft(binarySearchTree.find(175));
            expect(binarySearchTree.find(150).rightChild.value).to.equal(180);
        });
        it('should rotate the node with nested children', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.rotateNodeLeft(binarySearchTree.find(150));
            expect(binarySearchTree.rootNode.rightChild.value).to.equal(175);
        });
        it('should rotate the node with nested children and left right should be new', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.rotateNodeLeft(binarySearchTree.find(150));
            expect(binarySearchTree.find(150).rightChild.value).to.equal(170);
        });
    });
    describe('Rotate Right', () => {
        it('should rotate the root with only leaf children', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.rotateNodeRight(binarySearchTree.rootNode);
            expect(binarySearchTree.rootNode.value).to.equal(50);
        });
        it('should rotate the root with nested children', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.insert(190);
            binarySearchTree.rotateNodeRight(binarySearchTree.rootNode);
            expect(binarySearchTree.rootNode.value).to.equal(50);
        });
        it('should rotate the root with nested children and left right should be new', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.insert(190);
            binarySearchTree.rotateNodeRight(binarySearchTree.rootNode);
            expect(binarySearchTree.find(100).leftChild.value).to.equal(75);
        });
        it('should rotate the node with only leaf children', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.rotateNodeRight(binarySearchTree.find(175));
            expect(binarySearchTree.find(150).rightChild.value).to.equal(170);
        });
        it('should rotate the node with nested children', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.rotateNodeRight(binarySearchTree.find(150));
            expect(binarySearchTree.rootNode.rightChild.value).to.equal(125);
        });
        it('should rotate the node with nested children and left right should be new', () => {
            binarySearchTree.insert(50);
            binarySearchTree.insert(150);
            binarySearchTree.insert(25);
            binarySearchTree.insert(75);
            binarySearchTree.insert(175);
            binarySearchTree.insert(125);
            binarySearchTree.insert(20);
            binarySearchTree.insert(30);
            binarySearchTree.insert(70);
            binarySearchTree.insert(80);
            binarySearchTree.insert(120);
            binarySearchTree.insert(130);
            binarySearchTree.insert(170);
            binarySearchTree.insert(180);
            binarySearchTree.rotateNodeRight(binarySearchTree.find(150));
            expect(binarySearchTree.find(150).leftChild.value).to.equal(130);
        });
    });
});
