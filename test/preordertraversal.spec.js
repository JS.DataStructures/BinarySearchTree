import chai from 'chai';
import BinarySearchTree from '../src/BinarySearchTree';
import traverse from '../src/preordertraversal';

const expect = chai.expect;

describe('Pre-Order Depth Traversal', () => {
    let binaryTree;
    beforeEach(() => {
        binaryTree = new BinarySearchTree(100);
        binaryTree.insert(50);
        binaryTree.insert(150);
        binaryTree.insert(25);
        binaryTree.insert(75);
        binaryTree.insert(175);
        binaryTree.insert(125);
        binaryTree.insert(20);
        binaryTree.insert(30);
        binaryTree.insert(70);
        binaryTree.insert(80);
        binaryTree.insert(120);
        binaryTree.insert(130);
        binaryTree.insert(170);
        binaryTree.insert(180);
    });
    describe('Pre-Order Tree traversal', () => {
        it('should traverse the tree', () => {
            const traversedOutput = traverse(binaryTree.rootNode);
            expect(traversedOutput.toString()).to.equal('100,50,25,20,30,75,70,80,150,125,120,130,175,170,180');
        });
    });
});
