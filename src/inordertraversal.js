export default function traverse(rootNode) {
    let traversalOutput = [];
    if (rootNode === null) {
        return traversalOutput;
    }
    traversalOutput = traversalOutput.concat(traverse(rootNode.leftChild));
    traversalOutput.push(rootNode.value);
    traversalOutput = traversalOutput.concat(traverse(rootNode.rightChild));
    return traversalOutput;
}
