# Binary Search Tree

## Installation

```
npm install BinarySearchTree
```

## Usage

```
import BinarySearchTree from 'BinarySearchTree';
```

## API

```
insert (value): Insert a value into the binary tree.

getNewNode (value): Get a new node

getParentNode (value): Get parent node of a node with required value

rotateNodeLeft (node): Rotate a node to the left

rotateNodeRight (node): Rotate a node to the right

delete (value): Delete a node with the provided value

contains (value): Check if the value exists in the tree.

find (value): Get the node containing the value.

minimum: Get the minimum value of the tree.

maximum: Get the maximum value of the tree.

isBinarySearchTree: Check if the tree is a binary search tree or not.

height: Get height of the tree.

root: Get root node.
```
