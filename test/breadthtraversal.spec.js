import chai from 'chai';
import BinarySearchTree from '../src/BinarySearchTree';
import traverse from '../src/breadthtraversal';

const expect = chai.expect;

describe('Breadth Traversal', () => {
    let binaryTree;
    beforeEach(() => {
        binaryTree = new BinarySearchTree(100);
        binaryTree.insert(50);
        binaryTree.insert(150);
        binaryTree.insert(25);
        binaryTree.insert(75);
        binaryTree.insert(175);
        binaryTree.insert(125);
        binaryTree.insert(20);
        binaryTree.insert(30);
        binaryTree.insert(70);
        binaryTree.insert(80);
        binaryTree.insert(120);
        binaryTree.insert(130);
        binaryTree.insert(170);
        binaryTree.insert(180);
    });
    describe('Breadth Tree traversal', () => {
        it('should traverse the tree', () => {
            const traversedOutput = traverse(binaryTree);
            expect(traversedOutput.toString()).to.equal('100,50,150,25,75,125,175,20,30,70,80,120,130,170,180');
        });
    });
});